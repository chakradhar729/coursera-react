import React, { Component } from 'react';
import { Card, CardImg, CardTitle, CardText, Breadcrumb, BreadcrumbItem, Modal, Button, ModalBody, Row, Label, ModalHeader, Col } from 'reactstrap';
import { Link } from 'react-router-dom';
import { LocalForm, Control, Errors } from 'react-redux-form';
import { Loading } from './LoadingComponent';
import { baseUrl } from '../shared/baseUrl';
import { FadeTransform, Fade, Stagger } from 'react-animation-components'

function RenderComments({ comments, postComment, dishId }) {
    if (comments == null) {
        return (<div></div>)
    }
    const comm = comments.map(comment => {
        return (
            <Fade in>
                <li key={comment.id}>
                    <p>{comment.comment}</p>
                    <p>--{comment.author},

                {
                            new Intl.DateTimeFormat('en-US', {
                                year: 'numeric',
                                month: 'long',
                                day: '2-digit'
                            }).format(new Date(comment.date))
                        };
                 </p></li>
            </Fade>
        )
    })
    return (
        <div className="col-12 col-md-5 m-1">
            <h4> Comments </h4>
            <ul className="list-unstyled">
                <Stagger in>
                    {comm}
                </Stagger>
            </ul>
            <CommentForm dishId={dishId} postComment={postComment} />
        </div>
    )

}
function RenderDish({ dish }) {
    if (dish != null) {
        return (
            <div className="col-12 col-md-5 m-1 container">
                <FadeTransform in
                    transformProps={{
                        exitTransform: 'scale(0.5) translateY(-50%)'
                    }} >
                    <Card>
                        <CardImg width="100%" src={baseUrl + dish.image} alt={dish.name}></CardImg>
                        <CardTitle>{dish.name}</CardTitle>
                        <CardText>{dish.description}</CardText>
                    </Card>
                </FadeTransform>
            </div>
        )
    }
    else {
        return (<div></div>)
    }
}
function Dishdetail(props) {
    const dish = props.dish;
    if (props.isLoading) {
        return (
            <div className="container">
                <div className="row">
                    <Loading />
                </div>
            </div>
        );
    }
    else if (props.errMess) {
        return (
            <div className="container">
                <div className="row">
                    <h4>{props.errMess}</h4>
                </div>
            </div>
        );
    }
    if (dish == null) {
        return (<div></div>)
    }
    else {
        return (
            <div className="container">
                <div className="row">
                    <Breadcrumb>
                        <BreadcrumbItem><Link to='/menu'>Menu</Link></BreadcrumbItem>
                        <BreadcrumbItem active>{props.dish.name}</BreadcrumbItem>
                    </Breadcrumb>
                    <div className="col-12 ">
                        <h3>{props.dish.name}</h3>
                    </div>
                </div>
                <div className="row ">
                    <RenderDish dish={props.dish} />
                    <RenderComments comments={props.comments}
                        postComment={props.postComment}
                        dishId={props.dish.id} />
                </div>
            </div>
        )
    }
}



export default Dishdetail;

const required = (val) => val && val.length;
const maxLength = (len) => (val) => (!val || val.length <= len);
const minLength = (len) => (val) => (val) && (val.length > len);

export class CommentForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isModalOpen: false
        }
        this.toggleModal = this.toggleModal.bind(this);
    }
    toggleModal() {
        console.log("toggleModal");
        this.setState({ isModalOpen: !this.state.isModalOpen });
    }
    handlesubmit(values) {
        this.toggleModal();
        this.props.postComment(this.props.dishId, values.rating, values.author, values.comment);
    }
    render() {
        return (
            <div>
                <Button outline onClick={this.toggleModal} key={this.props.commentid}>
                    <i className="fa fa-pencil"> </i>
                    Submit Comments</Button>
                <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
                    <ModalHeader toggle={this.toggleModal}>Submit Comment</ModalHeader>
                    <ModalBody>
                        <LocalForm onSubmit={(values) => this.handlesubmit(values)}>
                            <Row className="form-group">
                                <Label htmlFor="rating" className="col-lg-12">Rating</Label>
                                <Col md={12}>
                                    <Control.select model=".rating"
                                        className="form-control"
                                        id="rating" name="rating">
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </Control.select>
                                </Col>
                            </Row>
                            <Row className="form-group">
                                <Label htmlFor="author" className="col-lg-12">Your Name</Label>
                                <Col md={12}>
                                    <Control.text model=".author" className="form-control"
                                        id="author" name="author" placeholder="Your Name"
                                        validators={{ required, minLength: minLength(2), maxLength: maxLength(15) }} />
                                    <Errors
                                        className="text-danger"
                                        model=".author"
                                        show="touched"
                                        messages={{
                                            required: 'Required',
                                            minLength: 'Must be greater than 2 characters',
                                            maxLength: 'Must be 15 characters or less'
                                        }}
                                    />

                                </Col>
                            </Row>
                            <Row className="form-group">
                                <Label htmlFor="comment" className="col-lg-12">Comment</Label>
                                <Col md={12}>
                                    <Control.textarea model=".comment" className="form-control"
                                        id="comment" name="comment" rows="6" />
                                </Col>
                            </Row>
                            <Row className="form-group">
                                <Col md={10} >
                                    <Button type="submit" color="primary">Submit</Button>
                                </Col>
                            </Row>
                        </LocalForm>
                    </ModalBody>
                </Modal>
            </div>
        );
    }
}